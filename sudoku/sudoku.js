$(document).ready(function() {

    let arr = [
        [9, 5, 4, 6, 2, 1, 7, 8, 3],
        [6, 7, 1, 4, 3, 8, 5, 2, 9],
        [2, 3, 8, 7, 5, 9, 4, 1, 6],
        [7, 9, 6, 2, 1, 5, 3, 4, 8],
        [1, 4, 5, 8, 7, 3, 6, 9, 2],
        [3, 8, 2, 9, 4, 6, 1, 7, 5],
        [4, 2, 9, 3, 6, 7, 8, 5, 1],
        [8, 1, 3, 5, 9, 4, 2, 6, 7],
        [5, 6, 7, 1, 8, 2, 9, 3, 4],
    ];

    // Print sudoku
    for(let i =0; i<9;i++) {
        for(let j = 0; j<9;j++) {
            document.write(arr[i][j]+ ' ');
        }
        document.writeln('<br>');
    }


    let rowAndCol = checkRowAndCol(arr);
    let validateBoxesData = validateBoxes(arr);

    document.writeln('<br>');
    if(rowAndCol && validateBoxes) {
        document.write('Valid Sudoku');
    } else {
        document.write('InValid Sudoku');
    }

    // Validate rows and columns
    function checkRowAndCol(arr) {
        let result;

        for (let i = 1; i <= 9; i++ ) {
            for (let j = 0; j < 9; j++ ) {
                result = false;
                for (let k = 0; k < 9; k++ ) {

                    if (arr[j][k] == i) {
                        result = true;

                    }

                    if (arr[k][j] == i && result == true) {
                        result = true;
                        break;
                    }
                }

                if (result == false) {
                    return false;
                }
            }

        }

        return true;
    }

    // Validate all boxes 3x3
    function validateBoxes(arr) {

        let checkBoxesData;
        for(let i =0; i<9;i+=3) {
            for(let j = 0; j<9;j+=3) {
                checkBoxesData = checkBoxes(arr, i, i+2, j, j+2);

                if (checkBoxesData == false) {
                    document.write(i + ' ' + j);
                    return false;
                }
            }
        }

        return true;
    }

    // Validate boxes 3x3
    function checkBoxes(arr, rowStart, rowEnd, colStart, colEnd) {
        let result;

        for (let i = 1; i <= 9; i++ ) {
            result = false;
            for (let j = rowStart; j <= rowEnd; j++ ) {
                for (let k = colStart; k <= colEnd; k++ ) {
                    if (arr[j][k] == i) {

                        result = true;
                        break;
                    }
                }
            }

            if (result == false) {
                return false;
            }
        }

        return true;
    }
});


