const mysql      = require('mysql');
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const dateFormat = require('dateformat');
const nodemailer = require('nodemailer');

const connection = mysql.createConnection({
    host     : process.env.DB_HOST,
    user     : process.env.DB_USERNAME,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_DATABASE
  });

connection.connect((err) => {
    if(err) {
        return;
    }
});

// Register API
exports.register = (req, res) => {
    if(!req.body.name || !req.body.email || !req.body.password) {
        return res.status(400).json({
            message: 'Enter all required fields'
        });
    }


    let query1 = 'Select id from users where email=? limit 1';

    connection.query(query1, [req.body.email], (err, result) => {
        if(err) {
            return res.status(500).json({
                message: 'Internal server error',
                error: err
            });
        }

        if (result.length > 0) {
            return res.status(400).json({
                message: 'Email already exists'
            });

        }
    });

    bcrypt.hash(req.body.password, 10, function(err, hash) {
        if(err) {
            return res.status(500).json({
                error: err
            });
        } else {
            let param = [req.body.name, req.body.email, hash];

            let query = 'Insert INTO users (name, email, password) VALUES (?)';

            connection.query(query, [param], (err, result) => {
                if(err) {
                    return res.status(500).json({
                        message: 'Internal server error',
                        error: err
                    });
                }

                return res.status(200).json({
                    message: 'User registered successfully'
                });

            });
        }
      });
};

// Login API
exports.login = (req, res) => {
    if(!req.body.email || !req.body.password) {
        return res.status(401).json({
            message: 'Enter email and password'
        });
    }

    let query1 = 'Select id, password from users where email=? limit 1';

    connection.query(query1, [req.body.email], (err, result) => {
        if(err) {
            return res.status(500).json({
                message: 'Internal server error',
                error: err
            });
        }

        if (result.length == 1) {
            bcrypt.compare(req.body.password, result[0]['password'], (err, result) => {
                if(err) {
                    return res.status(500).json({
                        message: 'Internal Server Error',
                        error: err
                    });
                }

                if(result) {
                    const token = jwt.sign({
                        email: req.body.email
                    }, 'secret', {
                        expiresIn: '1h'
                    });
                    return res.status(200).json({
                        'message': 'logged in',
                        'token': token
                    });
                } else {
                    return res.status(401).json({
                        message: 'Auth Fail'
                    });
                }
            });
        } else {
            return res.status(401).json({
                message: 'User does not exist with this email'
            });
        }
    });

};

// ForgotPassword API
exports.forgotPassword = (req, res) => {
    if(!req.body.email) {
        return res.status(401).json({
            message: 'Enter email'
        });
    }

    let otp = Math.floor(100000 + Math.random() * 900000);
    let now = dateFormat(new Date(), 'yyyy-mm-dd h:MM:ss');

    let query1 = 'Update users SET otp=? , otp_sent_at=? where email=? limit 1';

    connection.query(query1, [otp, now, req.body.email], (err, result) => {
        if(err) {
            return res.status(500).json({
                message: 'Internal server error',
                error: err
            });
        }

        if (result['affectedRows'] == 1) {

            const transporter = nodemailer.createTransport({
              service: 'gmail',
              auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
              }
            });

            const mailOptions = {
              from: process.env.MAIL_USERNAME,
              to: req.body.email,
              subject: 'OTP for reset Password',
              text: 'OTP is: ' + otp
            };

            transporter.sendMail(mailOptions, function(error, info){
              if (error) {
                return res.status(400).json({
                    message: 'OTP not sent here',
                    error: error
                });
              } else {
                return res.status(200).json({
                    message: 'OTP sent to email, please check',
                    otp: otp + now
                });

              }
            });
        } else {
            return res.status(400).json({
                message: 'OTP not sent'
            });
        }
    });
};

// ResetPassword API
exports.resetPassword = (req, res) => {
    if(!req.body.email || !req.body.otp || !req.body.password) {
        return res.status(401).json({
            message: 'Enter email'
        });
    }

    let query = 'Select id, otp_sent_at from users where email=? AND otp=? limit 1';


    connection.query(query, [req.body.email, req.body.otp], (err, result) => {
        if(err) {
            return res.status(500).json({
                message: 'Internal server error',
                error: err
            });
        }

        if (result.length == 1) {
            if(( new Date() - new Date(result[0]['otp_sent_at']))/(60*60*24) > 50) {
                return res.status(200).json({
                    message: 'OTP expired'
                });
            } else {
                bcrypt.hash(req.body.password, 10, function(hashErr, hash) {
                    if(hashErr) {
                        return res.status(500).json({
                            error: hashErr
                        });
                    } else {
                        let query1 = 'Update users SET password=?, otp=null , otp_sent_at=null where email=? limit 1';

                        connection.query(query1, [hash, req.body.email], (updateErr, updateRes) => {
                            if(updateErr) {
                                return res.status(500).json({
                                    message: 'Internal server error',
                                    error: updateErr
                                });
                            }

                            if (updateRes['affectedRows'] == 1) {

                                return res.status(200).json({
                                    message: 'Password changed'
                                });
                            } else {
                                return res.status(400).json({
                                    message: 'Password not changed'
                                });
                            }
                        });
                    }
                });
            }
        } else {
            return res.status(400).json({
                    message: 'Invalid email or OTP'
                });
        }
    });
};